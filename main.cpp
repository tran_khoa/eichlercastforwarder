#include <iostream>
#include <PcapLiveDevice.h>
#include <PcapLiveDeviceList.h>
#include <IPv4Layer.h>

using namespace pcpp;

struct VPNDeviceWrapper {
    PcapLiveDevice* vpn_device;
};

void onPacketReceive(RawPacket* pkt, PcapLiveDevice* dev, void* cookie) {
    Packet packet(pkt);

    VPNDeviceWrapper* settings = (VPNDeviceWrapper*) cookie;

    if (!packet.isPacketOfType(UDP))
        return;

    IPv4Layer* ipLayer = packet.getLayerOfType<IPv4Layer>();
    if(ipLayer != NULL) {
        if (dev->getIPv4Address() != ipLayer->getSrcIpAddress()) {
            return;
        }

        int prefix = std::stoi(ipLayer->getDstIpAddress().toString().substr(0, 3));
        bool is_broadcast = (ipLayer->getDstIpAddress().toString().c_str(), "255.255.255.255" != 0);

        if (prefix < 224 || (prefix > 239 && !is_broadcast)) {
            return;
        }

        ipLayer->setSrcIpAddress(settings->vpn_device->getIPv4Address());
    } else {
        return;
    }

    if(!settings->vpn_device->sendPacket(*pkt)) {
        puts("Broadcast konnte nicht weitergeleitet werden.\n");
    }
}

int main() {
    std::vector<PcapLiveDevice*> device_list = (&pcpp::PcapLiveDeviceList::getInstance())->getPcapLiveDevicesList();

    if (device_list.size() <= 1) {
        printf("%s", "Wahrscheinlich wurde das VPN-Interface noch nicht eingerichtet.");
    }

    PcapLiveDevice* main_device = NULL;
    while(main_device == NULL) {
        for (std::vector<int>::size_type i = 0; i != device_list.size(); i++) {
            printf("[%d] %s\n", i, device_list[i]->getDesc());
        }

        printf("%s", "Waehle das Haupt-Interface: ");
        int choice;
        std::cin >> choice;

        if (choice >= 0 && choice < device_list.size()) {
            main_device = device_list[choice];
            device_list.erase(device_list.begin() + choice);
        } else {
            printf("%s", "Ungueltige Auswahl.\n");
        }
    }

    PcapLiveDevice* vpn_device = NULL;
    if (device_list.size() == 1) {
        vpn_device = device_list[0];
    } else while(vpn_device == NULL) {
        for (std::vector<int>::size_type i = 0; i != device_list.size(); i++) {
            printf("[%d] %s\n", i, device_list[i]->getDesc());
        }

        printf("%s", "Waehle das VPN-Interface: ");
        int choice;
        std::cin >> choice;

        if (choice >= 0 && choice < device_list.size()) {
            vpn_device = device_list[choice];
        } else {
            printf("%s", "Ungueltige Auswahl.\n");
        }
    }

    VPNDeviceWrapper data;
    data.vpn_device = vpn_device;

    puts("Oeffne Haupt-Device.");
    if (!main_device->open()) {
        puts("Konnte das Haupt-Device nicht oeffnen.");
        return 1;
    }

    puts("Oeffne VPN-Device.");
    if (!vpn_device->open()) {
        puts("Konnte das VPN-Device nicht oeffnen.");
        return 2;
    }

    puts("Yay. Alle relevanten Pakete werden jetzt umgeleitet.");
    puts("Schreibe stop, um das Programm sauber zu beenden.");
    main_device->startCapture(onPacketReceive, &data);

    bool shouldStop = false;

    while (!shouldStop) {
        std::string s;
        std::cin >> s;

        if (strcmp(s.c_str(), "stop") == 0) shouldStop = true;
    }

    main_device->stopCapture();
    main_device->close();
    vpn_device->close();
    puts("Das Programm wurde sauber beendet.");
    return 0;
}

